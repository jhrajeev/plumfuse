package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import library.ProjectMethods;
import pages.Modules.Homepage;


public class TC001_CreateApp extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "Create an App";
		//testNodes = "Login";
		testDesc = "Create an App Process flow with SMS and Email";
		category = "smoke";
		authors = "Rajeev";
		dataSheetName = "Data_Create an App";
	}

	@Test(dataProvider = "fetchData")
	public void CreateAnApp(String AppName, String PhoneNum, String SMSText, String SMTP, String Port, String Username, String Password, String FromEmail, String ToEmail, String Subject, String EmailMsg) throws InterruptedException {
		
		//1. Go to url : http://quickfuseapps.com/
		new Homepage()
		
		//2. Click on Create an App
		.clickCreateAnApp()
		
		//3. You will land up on default page and then click �Lets� get started�
		.clickLetsGetStarted()
		
		//4. Create a new page and give it a name
		.clickNewPage(AppName)
		
		//5. Click on �Messaging� group appearing on the left pane under MODULES
		.clickMessaging()
		
		//6. Drag component �Send an SMS� to the main app page & join the line from start acting as connector
		.dragAndDropSendAnSMS().linkStartAndSMS()
		
		//7. Fill the details of Phone Number and Message text
		.enterPhoneNum(PhoneNum).enterSMSText(SMSText)
		
		
		//8. Drag component �Send an email� from the left module and join line from �Not sent� output port. 
		//Also fill all the details of the mail as shown
		.dragAndDropSendAnEmail().linkSMSAndEmail().enterSMTP(SMTP).enterPort(Port).enterUsername(Username).enterPassword(Password)
		.enterFromEmail(FromEmail).enterToEmail(ToEmail).enterEmailSubject(Subject).enterEmailMsg(EmailMsg)
		
		//9. Click on component �Basic� on left Module and drag �Exit App� joining to �Sent� output port of Sent an sms box
		.clickBasic().linkSMSSentToExit()
		
		//10. Similarly, Join all the open output ports of �Send an Email� to Exit app by dragging
		.linkEmailSentToExit().linkEmailNotSentToExit()
		
		//11.Test Screenshot
		.takeScreenShot();
		
	}
}