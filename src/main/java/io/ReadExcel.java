package io;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	public static Object[][] readexcel(String dataSheetName) throws IOException
	{
	 XSSFWorkbook wb = new XSSFWorkbook("./Data/"+dataSheetName+".xlsx");
	 XSSFSheet sheet = wb.getSheetAt(0);
	
	 int rowcount = sheet.getLastRowNum();
	 System.out.println(rowcount);
	 
	 int columnCount = sheet.getRow(0).getLastCellNum();	
	 System.out.println(columnCount);
	 
	 Object[][] data=new Object[rowcount][columnCount];
	 for (int j = 1; j<=rowcount; j++) {
		XSSFRow row = sheet.getRow(j);
		for (int i = 0; i < columnCount; i++) {
			XSSFCell cell = row.getCell(i);
			String Value = cell.getStringCellValue();
			System.out.println(Value);
			data[j-1][i]=Value;
			
		} 
	}
	 
	return data; 

	}
		
}