package library;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import io.Report;
import pages.Modules.Messaging;

public class SeMethods extends Report implements  InterfMethods{
	public int i = 1;
	public static RemoteWebDriver driver;
	public static Actions builder;

	public void startApp(String browser, String url) {
		
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			builder = new Actions(driver);
		} catch (WebDriverException e) {
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "LinkText": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			case "partiallink" : return driver.findElementByPartialLinkText(locValue);

			}
		} catch (NoSuchElementException e) {
		}
		return null;
	}

	public void gotoUrl(String webUrl) {
		driver.get(webUrl);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}


	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
		} catch (WebDriverException e) {

		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
		} catch (WebDriverException e) {
		}

	}

	public void clearText(WebElement ele) {
		try {
			ele.clear();
		} catch (WebDriverException e) {
		}

	}

	
	
	@Override
	public void drag(WebElement eleFrom, WebElement eleTo) throws InterruptedException {
		builder.dragAndDrop(eleFrom, eleTo).perform();
		Thread.sleep(3000);
	}
	
	@Override
	public void linkNodes(WebElement eleFromNode, WebElement eleToNode) throws InterruptedException { 
		builder.dragAndDrop(eleFromNode, eleToNode).release().perform();
		Thread.sleep(1000);
	}
	
	
	
	@Override
	public void hoverAndClick(WebElement eledefault, WebElement elehover) throws InterruptedException {
		builder.moveToElement(eledefault).perform();
		Thread.sleep(500);
		builder.moveToElement(elehover).perform();
		builder.click().build().perform();
	}
	
	
	@Override
	public boolean verifyTitle(String expectedTitle) {
		if(driver.getTitle().equals(expectedTitle)){
		}
		return false;
	}


	@Override
	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File dsc = new File("./reports/snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}


}
