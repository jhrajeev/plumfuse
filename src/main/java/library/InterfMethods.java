package library;

import java.net.MalformedURLException;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public interface InterfMethods {		

	/**
	 * This method will launch Any browser and 
	 * maximize the browser and set the wait for 30 seconds 
	 * and load the url
	 * @author Rajeev
	 * @param browser - This will load the specified browser  
	 * @throws MalformedURLException 
	 */
	public void startApp(String browser,String url);

	/**
	 * This method will locate the element using any given locator
	 * @param locator  - The locator by which the element to be found
	 * @param locValue - The locator value by which the element to be found
	 * @author Rajeev 
	 * @throws NoSuchElementException
	 */
	public WebElement locateElement(String locator, String locValue) ;	


	/**
	 * This method will enter the value in the given text field 
	 * @param ele   - The Webelement (text field) in which the data to be entered
	 * @param data  - The data to be sent to the webelement
	 * @author Rajeev 
	 * @throws ElementNotVisibleException		 * 
	 */
	public void type(WebElement ele, String data) ;
	
	
	/**
	 * This method will drag and drop the element FROM location to TO location. 
	 * @param ele   - Webelement (FROM and TO locators) for dragable
	 * @param data  - The data to be sent to the webelement
	 * @author Rajeev 
	 * @throws InterruptedException 
	 * @throws ElementNotVisibleException		 * 
	 */
	public void drag(WebElement eleFrom, WebElement eleTo) throws InterruptedException ;

	/**
	 * This method will click the element and take snap
	 * @param ele   - The Webelement (button/link/element) to be clicked
	 * @author Rajeev 
	 */
	public void click(WebElement ele);

	/**
	 * This method will verify browser actual title with expected
	 * @param title - The expected title of the browser
	 * @author Rajeev 
	 */
	public boolean verifyTitle(String expectedTitle);


	/**
	 * This method will take snapshot of the browser
	 * @author Rajeev 
	 */
	public void takeSnap();

	/**
	 * This method will close the active browser
	 * @author Rajeev 
	 */
	public void closeBrowser();		

	/**
	 * This method will hit the given URL in specified browser
	 * @author Rajeev 
	 */

	void gotoUrl(String webUrl);

	/**
	 * This method will hover the mouse to the element and then click 
	 * @author Rajeev 
	 * @throws InterruptedException 
	 */
	void hoverAndClick(WebElement eledefault, WebElement elehover) throws InterruptedException;

	
	/**
	 * This method will click and drag to connect the link 
	 * @author Rajeev 
	 */
	void linkNodes(WebElement eleFromNode, WebElement eleToNode) throws InterruptedException;
	
	


}