package library;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import io.ReadExcel;


public class ProjectMethods extends SeMethods{

	

	@BeforeSuite (groups = {"any"})
	public void beforeSuite() {
		startResult();
	}

	@BeforeMethod (groups = {"any"})
	public void LaunchURL() {
		beforeMethod();
		startApp("chrome", "http://quickfuseapps.com/");	
	}

	@AfterMethod (groups = {"any"})
	public void closeApp() {
		closeBrowser();
	}


	@AfterSuite (groups = {"any"})
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name="fetchData")
	public Object[][] readExcel() throws IOException {
		ReadExcel dp = new ReadExcel();
		Object[][] sheet = dp.readexcel(dataSheetName);
		return sheet;
	}



}