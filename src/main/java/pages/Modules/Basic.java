package pages.Modules;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import library.ProjectMethods;

public class Basic extends ProjectMethods{

	// Initiate the PageFactory 
	public Basic() {
		PageFactory.initElements(driver, this);
	}

	// Drag and Drop the SendAnSMS module to the right side box
	@FindBy(how = How.XPATH, using = "//li[@class='module-item ui-widget-content ui-corner-all module-item-red ui-draggable']") WebElement eleExit;
	@FindBy(how = How.XPATH, using = "//div[@class='ui-layout-center ui-layout-pane ui-layout-pane-center']/div[10]") WebElement eleToTab2;
	public Basic dragAndDropExit() throws InterruptedException {
		drag(eleExit, eleToTab2);
		Thread.sleep(1000);
		return this;	
	}
	
	// Link SMS-Sent to Exit (This can be done with taking coordinates (getlocation) also, id is used for dynamic value. This is only due to time constrains.
	@FindBy(how = How.XPATH, using = "//div[@id='module-2']/div/div[3]/div/div[3]/div[1]") WebElement eleFromSMSSent;
	@FindBy(how = How.XPATH, using = "//div[@id='module-4']/div[2]/div") WebElement eleToExitSMSSent;
	public Basic linkSMSSentToExit() throws InterruptedException {
		dragAndDropExit();			
		WebElement source = locateElement("id", "module-4");
		builder.dragAndDropBy(source, -200, +10).perform();
		Thread.sleep(1000);
		linkNodes(eleFromSMSSent, eleToExitSMSSent);
		return this;
	}
	
	// Link Email-Sent to Exit. (This can be done with taking coordinates (getlocation) also, id is used for dynamic value. This is only due to time constrains.
	@FindBy(how = How.XPATH, using = "//div[@id='module-3']/div/div[3]/div/div[4]/div[1]") WebElement eleFromEmailSent;
	@FindBy(how = How.XPATH, using = "//div[@id='module-5']/div[2]/div") WebElement eleToExitEmailSent;
	public Basic linkEmailSentToExit() throws InterruptedException {
		dragAndDropExit();
		WebElement source = locateElement("id", "module-5"); 
		builder.dragAndDropBy(source, -75, +100).perform();
		Thread.sleep(1000);
		linkNodes(eleFromEmailSent, eleToExitEmailSent);
		return this;
	}
	
	// Link Email-NotSent to Exit (This can be done with taking coordinates (getlocation) also, id is used for dynamic value. This is only due to time constrains.
	@FindBy(how = How.XPATH, using = "//div[@id='module-3']/div/div[3]/div/div[4]/div[2]") WebElement eleFromEmailNotSent;
	@FindBy(how = How.XPATH, using = "//div[@id='module-6']/div[2]/div") WebElement eleToExitEmailNotSent;
	public Basic linkEmailNotSentToExit() throws InterruptedException {
		dragAndDropExit();
		WebElement source = locateElement("id", "module-6"); 
		builder.dragAndDropBy(source, +500, +100).perform();
		Thread.sleep(3000);
		linkNodes(eleFromEmailNotSent, eleToExitEmailNotSent);
		return this;
	}
	
	public Basic takeScreenShot() {
		takeSnap();
		return this;	
	}
	
}