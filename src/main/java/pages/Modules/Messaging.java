package pages.Modules;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import library.ProjectMethods;

public class Messaging extends ProjectMethods{

	// Initiate the PageFactory 
	public Messaging() {
		PageFactory.initElements(driver, this);
	}

	// Drag and Drop the SendAnSMS module to the right side box. This can be done with taking coordinates (getlocation), this is only due to time constrains.
	@FindBy(how = How.XPATH, using = "(//li[@class='module-item ui-widget-content ui-corner-all module-item-green ui-draggable'])[3]") WebElement eleSendAnSMS;
	@FindBy(how = How.XPATH, using = "//div[@class='ui-layout-center ui-layout-pane ui-layout-pane-center']/div[10]") WebElement eleTo;
	public Messaging dragAndDropSendAnSMS() throws InterruptedException {
		drag(eleSendAnSMS, eleTo);
		Thread.sleep(1000);
		WebElement source = locateElement("xpath", "//div[contains(text(), 'Send an SMS')]"); 
		builder.dragAndDropBy(source, -34, -145).perform();
		Thread.sleep(1000);
		return this;	

	}


	// Link Start Module to Send an SMS
	@FindBy(how = How.XPATH, using = "//div[@id='module-1']/div[5]/div") WebElement eleFromStart;
	@FindBy(how = How.XPATH, using = "//div[@id='module-2']/div[2]/div") WebElement eleToSMS;
	public Messaging linkStartAndSMS() throws InterruptedException {
		linkNodes(eleFromStart, eleToSMS);
		return this;	

	}
	
	// Type a Number to Sent SMS
	@FindBy(how = How.NAME, using = "phone_constant") WebElement eleEnterSMSPhoneNum;
	public Messaging enterPhoneNum(String PhoneNum) throws InterruptedException {
		type(eleEnterSMSPhoneNum, PhoneNum);
		return this;	

	}
	
	// Type a Text to send SMS
	@FindBy(how = How.XPATH, using = "(//textarea[@name='message_phrase[]'])[1]") WebElement eleEnterSMSText;
	public Messaging enterSMSText(String SMSText) throws InterruptedException {
		type(eleEnterSMSText, SMSText);
		Thread.sleep(500);
		return this;	
	}


	// Drag and Drop Send an Email Module. This can be done with taking coordinates (getlocation), this is only due to time constrains.
	@FindBy(how = How.XPATH, using = "(//li[@class='module-item ui-widget-content ui-corner-all module-item-green ui-draggable'])[2]") WebElement eleSendAnEmail;
	@FindBy(how = How.XPATH, using = "//div[@class='ui-layout-center ui-layout-pane ui-layout-pane-center']/div[10]") WebElement eleEmailTo;
	public Messaging dragAndDropSendAnEmail() throws InterruptedException {
		drag(eleSendAnEmail, eleEmailTo);
		Thread.sleep(1000);
		WebElement module3 = locateElement("xpath", "//div[contains(text(), 'Send an Email')]"); 
		builder.dragAndDropBy(module3, +175, -25).perform();
		Thread.sleep(1000);
		return this;	
	}

	
	// Link SMS not Sent to Email Module
	@FindBy(how = How.XPATH, using = "//div[@id='module-2']/div/div[3]/div/div[3]/div[2]") WebElement eleFromSMS;
	@FindBy(how = How.XPATH, using = "//div[@id='module-3']/div[2]/div") WebElement eleToEmailModule;
	public Messaging linkSMSAndEmail() throws InterruptedException {
		linkNodes(eleFromSMS, eleToEmailModule);
		return this;
	}
	
	// Type a SMTP Server to send Email
	@FindBy(how = How.NAME, using = "smtp_url") WebElement eleSMTP;
	public Messaging enterSMTP(String SMTP) {
		type(eleSMTP, SMTP);
		return this;	
	}
	
	// Type a Port number to send Email
	@FindBy(how = How.NAME, using = "port") WebElement elePort;
	public Messaging enterPort(String Port) {
		type(elePort, Port);
		return this;	
	}
	
	// Type a Username to send Email
	@FindBy(how = How.NAME, using = "username") WebElement eleUsername;
	public Messaging enterUsername(String Username) {
		type(eleUsername, Username);
		return this;	
	}
	
	// Type a Password to send Email
	@FindBy(how = How.NAME, using = "password") WebElement elePassword;
	public Messaging enterPassword(String Password) {
		type(elePassword, Password);
		return this;	
	}
	
	// Type a From Email to send Email
	@FindBy(how = How.XPATH, using = "//textarea[@name='from_constant']") WebElement eleFromEmail;
	public Messaging enterFromEmail(String FromEmail) {
		type(eleFromEmail, FromEmail);
		return this;	
	}
	
	// Type a TO Email to send Email
	@FindBy(how = How.XPATH, using = "//textarea[@name='to_constant']") WebElement eleToEmail;
	public Messaging enterToEmail(String ToEmail) {
		type(eleToEmail, ToEmail);
		return this;	
	}
	
	// Type Subject of the Email
	@FindBy(how = How.XPATH, using = "//textarea[@name='subject_constant']") WebElement eleSubject;
	public Messaging enterEmailSubject(String Subject) {
		type(eleSubject, Subject);
		return this;	
	}
	
	// Type Email Message 
	@FindBy(how = How.XPATH, using = "(//textarea[@name='message_phrase[]'])[3]") WebElement eleEmailMsg;
	public Messaging enterEmailMsg(String EmailMsg) {
		type(eleEmailMsg, EmailMsg);
		return this;	
	}
	
	// Click 'Basic' link at left side module to expand.	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Basic") WebElement eleBasic;
	public Basic clickBasic() throws InterruptedException {
		click(eleBasic);
		Thread.sleep(2000);
		return new Basic();	
	}
	
	public Messaging takeScreenShot() {
		takeSnap();
		return this;	
	}

}