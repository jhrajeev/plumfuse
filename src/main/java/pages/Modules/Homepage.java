package pages.Modules;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import library.ProjectMethods;

public class Homepage extends ProjectMethods{

	// Initiate the PageFactory 
	public Homepage() {
		PageFactory.initElements(driver, this);
	}

	// Click 'Create an App' button at homepage
	@FindBy(how = How.LINK_TEXT, using = "Create an App") WebElement eleCreateAnApp;
	public ModuleFacets clickCreateAnApp()  {
		click(eleCreateAnApp);
		return new ModuleFacets();	
	}
	
	
	public Homepage takeScreenShot() {
		takeSnap();
		return this;	
	}


	
}