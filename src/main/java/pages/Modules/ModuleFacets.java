package pages.Modules;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import library.ProjectMethods;

public class ModuleFacets extends ProjectMethods{

	// Initiate the PageFactory 
	public ModuleFacets() {
		PageFactory.initElements(driver, this);
	}

	// Click 'Let's Get Started' button on the Lightbox in Edit page.
	@FindBy(how = How.XPATH, using = "(//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'])[1]/button") WebElement eleLetsGetStartedDefault;
	@FindBy(how = How.XPATH, using = "//button[@class='ui-corner-all done ui-button ui-state-hover']") WebElement eleLetsGetStartedHover;
	public ModuleFacets clickLetsGetStarted() throws InterruptedException {
		hoverAndClick(eleLetsGetStartedDefault, eleLetsGetStartedHover);
		return this;	
	}

	// Create 'New Page'
	@FindBy(how = How.ID, using = "add-page") WebElement eleAddPage;
	@FindBy(how = How.XPATH, using = "//input[@class='indented submitonenter']") WebElement eleLetsStart;
	@FindBy(how = How.XPATH, using = "(//*[contains(text(), 'Create')])[2]") WebElement eleCreateButtonDefault;
	@FindBy(how = How.XPATH, using = "//*[@class='ui-corner-all ui-button ui-state-hover']") WebElement eleCreateButtonHover;
	public ModuleFacets clickNewPage(String PageName) throws InterruptedException  {
		click(eleAddPage);
		type(eleLetsStart, PageName);
		hoverAndClick(eleCreateButtonDefault, eleCreateButtonHover);
		return this;	
	}	


	// Click 'Messaging' link at left side module to expand.	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Messaging") WebElement eleMessaging;
	public Messaging clickMessaging() throws InterruptedException {
		click(eleMessaging);
		Thread.sleep(2000);
		return new Messaging();	
	}  

	// Click 'Basic' link at left side module to expand.	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Basic") WebElement eleBasic;
	public Basic clickBasic() throws InterruptedException {
		click(eleBasic);
		Thread.sleep(2000);
		return new Basic();	
	}
	
	public ModuleFacets takeScreenShot() {
		takeSnap();
		return this;	
	}

}